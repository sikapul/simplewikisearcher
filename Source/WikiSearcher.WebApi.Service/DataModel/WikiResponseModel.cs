﻿using Newtonsoft.Json;

namespace WikiSearcher.WebApi.Service.DataModel
{
    [JsonObject(MemberSerialization = MemberSerialization.OptIn)]
    internal class WikiResponseModel
    {
        [JsonProperty("batchcomplete")]
        public string Batchcomplete;

        [JsonProperty("query")]
        public Query Query;
    }
}