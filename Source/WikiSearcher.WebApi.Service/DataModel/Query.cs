﻿using System.Collections.Generic;
using Newtonsoft.Json;

namespace WikiSearcher.WebApi.Service.DataModel
{
    [JsonObject(MemberSerialization = MemberSerialization.OptIn)]
    internal class Query
    {
        [JsonProperty("pages")]
        public Dictionary<string, Page> Pages;
    }
}