﻿using Newtonsoft.Json;

namespace WikiSearcher.WebApi.Service.DataModel
{
    [JsonObject(MemberSerialization = MemberSerialization.OptIn)]
    internal class Revisions
    {
        [JsonProperty("contentformat")]
        public string Contentformat;

        [JsonProperty("contentmodel")]
        public string Contentmodel;

        [JsonProperty(PropertyName = "*")]
        public string WikiMarkupText;
    }
}