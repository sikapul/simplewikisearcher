using Newtonsoft.Json;

namespace WikiSearcher.WebApi.Service.DataModel
{
    [JsonObject(MemberSerialization = MemberSerialization.OptIn)]
    internal class Page
    {
        [JsonProperty("pageid")]
        public int PageId;

        [JsonProperty("ns")]
        public int Ns;

        [JsonProperty("title")]
        public string Title;

        [JsonProperty("revisions")]
        public Revisions[] Revisions;
    }
}