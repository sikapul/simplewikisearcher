﻿using System;
using System.IO;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using Newtonsoft.Json;
using WikiSearcher.DataModel.Exceptions;
using WikiSearcher.DataModel.Interfaces;
using WikiSearcher.WebApi.Service.DataModel;

namespace WikiSearcher.WebApi.Service
{
    public class WebWikiRepositoryReader : IWikiRepositoryReader
    {
        private const string EndPointUrl = "https://ru.wikipedia.org/w/api.php?";

        public async Task<bool> IsPageExist(string query)
        {
            var request = string.Format(
                 "{0}action=query&titles={1}&prop=info&format=json", EndPointUrl, query);
            return await DoGetRequest(request, RespounceHavePageId);
        }

        private static bool RespounceHavePageId(WikiResponseModel responceModel)
        {
            return responceModel.Query != null && responceModel.Query.Pages != null && responceModel.Query.Pages.Count > 0 && responceModel.Query.Pages.First().Value.PageId > 0;
        }

        public async Task<string> GetWikiMarkupText(string query)
        {
            var request = string.Format(
                "{0}action=query&titles={1}&prop=revisions&rvprop=content&format=json", EndPointUrl, query);
            return
                await
                    DoGetRequest(request,
                        responceModel =>
                        {
                            try
                            {
                                return responceModel.Query.Pages.First().Value.Revisions.First().WikiMarkupText;
                            }
                            catch (ArgumentNullException e)
                            {
                                throw new DataNotFoundException(
                                    string.Format("Не удалось найти страницу по запосу {0}", query), e);
                            }
                        });
        }

        public async Task<int> GetPageId(string query)
        {
            var request = string.Format(
                 "{0}action=query&titles={1}&prop=info&format=json", EndPointUrl, query);
            return await DoGetRequest(request,
                responceModel =>
                {
                    try
                    {
                        var pageId = responceModel.Query.Pages.First().Value.PageId;
                        if (pageId > 0) return pageId;
                        throw new DataNotFoundException(
                            string.Format("Не удалось получить идентификатор страницы по запросу {0}", query));
                    }
                    catch (ArgumentNullException e)
                    {
                        throw new DataNotFoundException(
                            string.Format("Не удалось получить идентификатор страницы по запросу {0}", query), e);
                    }
                });
        }

        private static async Task<T> DoGetRequest<T>(string getRequest, Func<WikiResponseModel, T> responseModelReader)
        {
            var webRequest = WebRequest.Create(getRequest);
            using (var response = await webRequest.GetResponseAsync())
            {
                using (var stream = response.GetResponseStream())
                {
                    if(stream == null)
                        throw new DataNotFoundException("Не удалось получить ответ от сервера");
                    using (var reader = new StreamReader(stream))
                    {
                        var responceModel = JsonConvert.DeserializeObject<WikiResponseModel>(await reader.ReadToEndAsync());
                        return responseModelReader(responceModel);
                    }
                }
            }
        }
    }
}
