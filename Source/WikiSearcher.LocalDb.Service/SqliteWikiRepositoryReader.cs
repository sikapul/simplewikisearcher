﻿using System.Data.Entity;
using System.IO;
using System.Threading.Tasks;
using WikiSearcher.DataModel.Exceptions;
using WikiSearcher.DataModel.Interfaces;
using WikiSearcher.LocalDb.Service.DataModel;

namespace WikiSearcher.LocalDb.Service
{
    public class SqliteWikiRepositoryReader : IWikiRepositoryReader, IWikiRepositoryWriter
    {
        public SqliteWikiRepositoryReader()
        {
            RestoreDbIfNotExist();
        }

        public async Task<bool> IsPageExist(string query)
        {
            using (var db = new SimpleCacheEntities())
            {
                var loverQuery = query.ToLower();
                return await db.WikiPage.AnyAsync(item => item.PageTitle == loverQuery);
            }
        }

        public async Task<string> GetWikiMarkupText(string query)
        {
            using (var db = new SimpleCacheEntities())
            {
                var loverQuery = query.ToLower();
                var page = await db.WikiPage.FirstOrDefaultAsync(item => item.PageTitle == loverQuery);
                if (page != null)
                    return page.WikiMarkup;
            }
            throw new DataNotFoundException(GetNotFoundMessage(query));
        }

        public async Task<int> GetPageId(string query)
        {
            using (var db = new SimpleCacheEntities())
            {
                var loverQuery = query.ToLower();
                var page = await db.WikiPage.FirstOrDefaultAsync(item => item.PageTitle == loverQuery);
                if (page != null)
                    return (int)page.PageId;
            }
            throw new DataNotFoundException(GetNotFoundMessage(query));
        }

        private static string GetNotFoundMessage(string query)
        {
            return string.Format("Не удалось найти данные в базе по запросу {0}", query);
        }

        public async Task WritePage(int pageId, string markupText, string title)
        {
            using (var db = new SimpleCacheEntities())
            {
                db.WikiPage.Add(new WikiPage
                {
                    PageId = pageId,
                    PageTitle = title.ToLower(),
                    WikiMarkup = markupText
                });
                await db.SaveChangesAsync();
            }
        }

        private static void RestoreDbIfNotExist()
        {
            if(!File.Exists("SimpleCache.db"))
                File.WriteAllBytes("SimpleCache.db", Properties.Resource.SimpleCache);
        }
    }
}
