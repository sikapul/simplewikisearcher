﻿using System;

namespace WikiSearcher.DataModel.Exceptions
{
    public class DataNotFoundException : Exception
    {
        public DataNotFoundException(string message, Exception innerException) : base(message, innerException)
        {
        }

        public DataNotFoundException(string message)
            : base(message)
        {
        }
    }
}
