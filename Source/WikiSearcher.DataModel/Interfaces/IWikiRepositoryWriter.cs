﻿using System.Threading.Tasks;

namespace WikiSearcher.DataModel.Interfaces
{
    public interface IWikiRepositoryWriter
    {
        Task WritePage(int pageId, string markupText, string title);
    }
}
