﻿using System.Threading.Tasks;

namespace WikiSearcher.DataModel.Interfaces
{
    public interface IWikiRepositoryReader
    {
        Task<bool> IsPageExist(string query);

        Task<string> GetWikiMarkupText(string query);

        Task<int> GetPageId(string query);
    }
}
