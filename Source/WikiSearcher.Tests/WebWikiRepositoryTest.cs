﻿using System.Threading.Tasks;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using WikiSearcher.DataModel.Exceptions;
using WikiSearcher.DataModel.Interfaces;
using WikiSearcher.WebApi.Service;

namespace WikiSearcher.Tests
{
    [TestClass]
    public class WebWikiRepositoryTest
    {
        private const string GoodQuery = "Москва";
        private const string BadQuery = "hkshdfkdsjh";

        private static IWikiRepositoryReader CreateRepository()
        {
            return new WebWikiRepositoryReader();
        }

        [TestMethod]
        public async Task TestExistPage()
        {
            var repository = CreateRepository();
            var isPageExist = await repository.IsPageExist(GoodQuery);
            Assert.IsTrue(isPageExist);
        }

        [TestMethod]
        public async Task TestGetWikiMarkup()
        {
            var repository = CreateRepository();
            var markupText = await repository.GetWikiMarkupText(GoodQuery);
            Assert.IsFalse(string.IsNullOrWhiteSpace(markupText));
        }

        [TestMethod]
        public async Task TestGetPageId()
        {
            var repository = CreateRepository();
            var pageId = await repository.GetPageId(GoodQuery);
            Assert.IsTrue(pageId > 0);
        }

        [TestMethod]
        [ExpectedException(typeof (DataNotFoundException))]
        public async Task TestGetWikiMarkupWithDataNotFoundException()
        {
            var repository = CreateRepository();
            await repository.GetWikiMarkupText(BadQuery);
        }

        [TestMethod]
        [ExpectedException(typeof(DataNotFoundException))]
        public async Task TestGetPageIdWithDataNotFoundException()
        {
            var repository = CreateRepository();
            await repository.GetPageId(BadQuery);
        }

        [TestMethod]        
        public async Task TestPageNotExist()
        {
            var repository = CreateRepository();
            Assert.IsFalse(await repository.IsPageExist(BadQuery));
        }
    }
}
