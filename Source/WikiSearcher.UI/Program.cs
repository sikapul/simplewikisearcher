﻿using System;
using System.Windows.Forms;
using WikiSearcher.WebApi.Service;

namespace WikiSearcher.UI
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            Application.Run(new WikiForm(new WebWikiRepositoryReader()));
        }
    }
}
