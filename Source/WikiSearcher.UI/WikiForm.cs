﻿using System.Net;
using System.Threading.Tasks;
using System.Windows.Forms;
using WikiSearcher.DataModel.Exceptions;
using WikiSearcher.DataModel.Interfaces;

namespace WikiSearcher.UI
{
    public partial class WikiForm : Form
    {
        private readonly IWikiRepositoryReader _wikiRepository;
        private string Query { get { return _tb_query.Text; } }

        public WikiForm(IWikiRepositoryReader wikiRepository)
        {
            InitializeComponent();
            _wikiRepository = wikiRepository;
        }

        private async void _buttonSearch_Click(object sender, System.EventArgs e)
        {
            await SearchWikiText();
        }

        private async Task SearchWikiText()
        {
            try
            {
                await RunSearch(Query);
            }
            catch (DataNotFoundException exception)
            {
                MessageBox.Show(exception.Message, @"Внимание", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            catch (WebException)
            {
                MessageBox.Show(@"Не удалось подключиться к интернету", @"Ошибка", MessageBoxButtons.OK,
                    MessageBoxIcon.Error);
            }
        }

        private async Task RunSearch(string query)
        {
            try
            {
                StartProgress();
                var text = await _wikiRepository.GetWikiMarkupText(query);
                _richTextBox.Text = text;
            }
            finally
            {
                StopProgress();
            }
        }

        private void StartProgress()
        {
            _progressBar.Visible = true;
            DisableControls();
        }

        private void StopProgress()
        {
            _progressBar.Visible = false;
            EnableControls();
        }

        private void DisableControls()
        {
            _richTextBox.Enabled = _tb_query.Enabled = _buttonSearch.Enabled = false;
        }

        private void EnableControls()
        {
            _richTextBox.Enabled = _tb_query.Enabled = _buttonSearch.Enabled = true;
        }

        private async void _tb_query_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
                await SearchWikiText();
        }
    }
}
